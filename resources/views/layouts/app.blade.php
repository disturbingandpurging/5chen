<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<title>@yield('title') - 5chen</title>
	<link rel="stylesheet" href="{{ url('css/app.css') }}">
</head>
<body>
	
	<header>
		<div class="Header__top">

			@if(!auth()->check())
			<a href="{{ url('login') }}">Login</a>
			<a href="{{ url('register') }}">Register</a>
			@endif

		</div>
		<div class="Header__bottom">
			<h1 class="Logo">5chen</h1>
			<h2>@yield('title')</h2>
		</div>
	</header>

	<main class="Board">
		@yield('content')
	</main>

	<footer>
		<p>&copy; 5chen, {{ date('Y') }}.</p>
	</footer>

</body>
</html>