<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Thread extends Model
{
    protected $fillable = ['user_id', 'post_id', 'board_id'];

    /**
     * A thread belongs to a board.
     */
    public function board()
    {
    	return $this->belongsTo('App\Board');
    }

    /**
     * A thread has a main post
     */
    public function post()
    {
    	return $this->hasOne('App\Post');
    }

    /**
     * A thread belongs to a user.
     */
    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}
